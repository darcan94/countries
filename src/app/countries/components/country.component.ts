import { Component, OnInit  } from "@angular/core"; 
import { Country } from "../country.model";
import { CountriesService } from "../services/countries.service";

@Component({
    selector: 'app-country',
    templateUrl:'./country.component.html',
    styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit{

    countries: Country[] = [];

    constructor(
        private countriesService: CountriesService
    ){}
    
    ngOnInit(){
        this.fetchCountries();
    }

    fetchCountries(){
        this.countriesService.getAllCountries().subscribe(countries => {
           this.countries = countries;
           console.log(countries);
        })
    }

    fetchCountryByName(name: string){
        this.countriesService.getCountry(name).subscribe(country => {
            console.log(country);
        })
    }
}