export interface Country{
    name: string;
    code: string;
    capital: string;
    region: string;
    population:number;
    latlong: number[];
    timezones: string[];
    borders: string[];
    currencies: string[];
    language: string[];
    flag: string;
}