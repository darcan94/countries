import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Country } from "../country.model";

@Injectable({
    providedIn: 'root'
})
export class CountriesService {
    constructor(
        private http: HttpClient
    ) {

    }

    getAllCountries(){
        return this.http.get<Country[]>('https://restcountries.eu/rest/v2/all');
    }

    getCountry(id: string){
        return this.http.get<Country>(`https://restcountries.eu/rest/v2/name/${id}`);
    }
}